import argparse
from RabbitMQ.RabbitMQManager import RabbitMQManager

from projects.brightmlshomes_1.LinksExtractor import LinksExtractor as LinksExtractor_1
from projects.realtor_2.LinksExtractor import LinksExtractor as LinksExtractor_2
from projects.homes_3.LinksExtractor import LinksExtractor as LinksExtractor_3
from projects.miamirealestate_5 import LinksExtractor as LinksExtractor_5


def get_data(project_id):
    rabbit = RabbitMQManager(project_id)
    rabbit.startConsuming()

def get_urls(project_id):
    if project_id == 1:
        worker = LinksExtractor_1()
    elif project_id == 2:
        worker = LinksExtractor_2()
    elif project_id == 3:
        worker = LinksExtractor_3()
    elif project_id == 5:
        worker = LinksExtractor_5
    else:
        raise
    worker.main()



parser = argparse.ArgumentParser()

parser.add_argument("project_id", type=int,
                    help="Please provide project id")
parser.add_argument("-l", "--links", action="store_true",
                    help="Extract links")
parser.add_argument("-d", "--data", action="store_true",
                    help="Extract data")
args = parser.parse_args()

if args.links and not args.data:
    get_urls(args.project_id)
elif args.data and not args.links:
    get_data(args.project_id)
else:
    raise
