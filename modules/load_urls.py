from RabbitMQ.RabbitMQManager import RabbitMQManager
from database.DatabaseManager import DatabaseManager


def load_urls(project_id):
    db = DatabaseManager()
    rabbit = RabbitMQManager(project_id)
    urls = db.get_urls(project_id=project_id)
    rabbit.load_urls(urls)
    print "Set in queue ", len(urls)


if __name__ == '__main__':
    load_urls(project_id=9)