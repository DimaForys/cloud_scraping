import csv
import re

from database.DatabaseManager import DatabaseManager
from managers.Generator import Generator

class FileGenerator:
    def __init__(self, project_id):
        self.db = DatabaseManager()
        self.generator = Generator()
        self.project_id = project_id
        self.project_title = self.db.get_project_name(project_id)
        self.date = self.generator.generate_timestamp(format="%Y.%m.%d")
        self.path = self.generator.generate_path_to_project()

    def generate_csv(self, session_name=None):
        filename = self.generator.generate_filename(self.project_title,
                                                    self.project_id,
                                                    self.date,
                                                    'csv')
        if session_name is None:
            session_name = self.generator.generate_session_name(self.project_id)
        data_list = self.db.get_data(self.project_id, session_name)
        if len(data_list) == 0:
            return
        with open(self.path + 'data/csv/' + filename, 'w') as opened_file:
            opened_csv = csv.writer(opened_file)

            print len(data_list)

            for item in data_list:
                url = item['url']
                timestamp = item['timestamp']
                data = item['data']
                row = []

                for column in data:
                    if type(data[column]) == int:
                        row.append(data[column])
                    else:
                        row.append(data[column].encode('utf-8'))

                row.append(url)
                row.append(timestamp)
                try:
                    opened_csv.writerow(row)
                except UnicodeEncodeError as exc:
                    print item, exc

    def __clean_string(self, string):
        string = re.sub(r'[^\x00-\x7f]', r'', str(string))
        return string

    def generate_json(self, project_id):
        pass

if __name__ == '__main__':
    file_generator = FileGenerator(project_id=8)
    file_generator.generate_csv()