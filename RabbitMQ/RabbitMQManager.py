import json
import multiprocessing
import threading
import rabbitpy

from RabbitMQConnector import RabbitMQConnector
from projects.brightmlshomes_1.DataExtractor import DataExtractor as brightmlhomesDataExtractor
from projects.realtor_2.DataExtractor import DataExtractor as realtorDataExtractor
from projects.homes_3.DataExtractor import DataExtractor as homesDataExtractor
from projects.miamirealestate_5.DataExtractor import DataExtractor as miamiRealtorsDataExtractor
from projects.katielarsonrealtor_6.DataExtractor import DataExtractor as katielarsonrealtorDataExtractor
from projects.clutch_7.DataExtractor import DataExtractor as clutchDataExtractor
from projects.movoto_8.DataExtractor import DataExtractor as movotoDataExtractor
from projects.im_scr_globewest_9.DataExtractor import DataExtractor as img_scraper_globewestDataExtractor

class RabbitMQManager():
    EXCHANGE = 'input'
    QUEUE = 'listings_urls'
    ROUTING_KEY = 'url'

    def __init__(self, project_id):
        self.connector = RabbitMQConnector()
        self.connection = self.connector.connect()
        self.kwargs = {'connection': self.connection}
        self.publishChannel = self.connection.channel()
        self.project_id = project_id

    def startConsuming(self):
        self.choose_project()
        with self.connection.channel() as channel:
            print "Channel"
            channel.prefetch_count(1)
            for message in rabbitpy.Queue(channel, self.QUEUE):
                try:
                    bodyMessage = message.body
                    bodyMessage = json.loads(bodyMessage)
                    print (" [x] %r received %r" % (threading.currentThread(), bodyMessage))
                    result = self.processMessage(bodyMessage)
                    if result is False:
                        self.load_urls([bodyMessage])
                    message.ack()
                except Exception as exc:
                    print exc

                    message.ack()

    def load_urls(self, urls):
        with self.connection.channel() as channel:
            for url in urls:
                message = rabbitpy.Message(channel=channel,
                                               body_value=url,
                                               properties={
                                                           "delivery_mode": 2},
                                               )
                message.publish(self.EXCHANGE, self.ROUTING_KEY)

    def choose_project(self):
        if self.project_id == 1:
            self.domainExtractor = brightmlhomesDataExtractor()
        elif self.project_id == 2:
            self.domainExtractor = realtorDataExtractor()
        elif self.project_id ==3:
            self.domainExtractor = homesDataExtractor()
        elif self.project_id == 5:
            self.domainExtractor = miamiRealtorsDataExtractor()
        elif self.project_id == 6:
            self.domainExtractor = katielarsonrealtorDataExtractor()
        elif self.project_id == 7:
            self.domainExtractor = clutchDataExtractor()
        elif self.project_id == 8:
            self.domainExtractor = movotoDataExtractor()
        elif self.project_id == 9:
            self.domainExtractor = img_scraper_globewestDataExtractor()

    def processMessage(self, message):
        try:
            self.domainExtractor.extract_data(message)
            return True
        except Exception as exc:
            print exc
            return False

def multiprocess(project_id):
    rabbit = RabbitMQManager(project_id)
    rabbit.startConsuming()




if __name__ == "__main__":
    multiprocess(project_id=9)