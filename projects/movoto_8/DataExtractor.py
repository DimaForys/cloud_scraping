import random
import time

from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator




class DataExtractor:

    DOMAIN = 'movoto.com'
    PROJECT_ID = 8

    def __init__(self):
        print self.DOMAIN
        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)


    def __del__(self):
        print "Closing driver"
        #self.driver.quit()

    def extract_data(self, url_data, debug=False):
        url_id = url_data['id']
        url = url_data['url']
        time.sleep(random.uniform(0.8,1.3))
        data = {}
        response = self.request_manager.take_get_request(url=url, proxy_using=False)
        source_code = response['source_code']
        parsed_code = self.source_code_manager.parse_code(source_code)
        proxy = response['proxy']

        contact_info = self.find_contact_info(parsed_code)
        agent_name = contact_info['agent_name']
        agency = contact_info['agency']
        mls = contact_info['msl']
        address = self.findAddress(parsed_code)
        price = self.findPrice(parsed_code)
        days = self.findAmountOfDays(parsed_code)
        status = self.find_status(parsed_code)


        data['price'] = price
        data['street'] = address['street']
        data['address'] = address['address']
        data['agent_name'] = agent_name
        data['agency'] = agency
        data['mls'] = mls
        data['days'] = days
        data['status'] = status

        data_for_db = self.generator.generate_data_to_database(data_from_page=data,
                                                               proxy=proxy,
                                                               url_id=url_id)
        print data_for_db
        if debug:
            return
        self.db.insert_data(data_for_db, self.PROJECT_ID, self.session_name)
        self.db.set_url_processed(url_id)

    def findAmountOfDays(self, code):
        try:
            info = code.find('div', {'class': 'f3'})
            if 'Just Listed' in info.text:
                days = 0
            else:
                days = info.findAll('span')[-1].text
        except Exception as e:
            print e
            days = ''
        return days

    def find_contact_info(self, code):
        block = code.find('div', {'class':'dpp-infolist text-light'})
        spans = block.findAll('span')
        try:
            mls = spans[0].text
        except:
            mls = ''
        try:
            agent_name = spans[1].text.replace(' of', '')
        except:
            agent_name = ''

        try:
            agency = spans[2].text
        except:
            agency = ''

        return dict(agent_name=agent_name,
                    msl=mls,
                    agency=agency)

    def findAddress(self, code):
        try:
            address_str = ''
            address = code.find('h1', {'itemprop':'address'})
            street = address.find('div').text.strip()
            spans = address.findAll('span')
            for span in spans:
                address_str += span.text.strip() + ' '
            address_str = address_str[:-1]
        except AttributeError:
            street = ''
            address_str = ''
        return {'street':street,
                'address':address_str}


    def findPrice(self, code):
        try:
            price = code.find('span', {'class':'price'}).text.replace(',','')
        except AttributeError:
            price = ""
        return price.strip()

    def find_status(self, code):
        try:
            status = code.find('div', {'class': 'dpp-hotlead-title-light'}).text
        except AttributeError:
            status = ''

        return status


if __name__ == '__main__':
    DataExtractor().extract_data({'id':1,
                                  'url':'https://www.movoto.com/home/950-n-michigan-ave-apt-4205-chicago-il-60611-461_09349422'},
                                 debug=True)