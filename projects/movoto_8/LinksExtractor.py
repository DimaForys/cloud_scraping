
from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator

class LinksExtractor:

    DOMAIN = 'movoto.com'
    PROJECT_ID = 8

    def __init__(self):
        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)


    def __createUrl(self, templateUrl, page):
        #url = templateUrl[:-1] + str(page)
        url = templateUrl.format(page)
        return url

    def findLinks(self, sourceCode):
        links = []
        listOfTags = sourceCode.findAll('div', {'class': 'cardtwo cardtwo-box '})
        for block in listOfTags:
            tag_a = block.find('a')
            href = tag_a['href']

            links.append(href)
        return links

    def main(self, sourceUrl):
        page = 1
        url = self.__createUrl(sourceUrl, page)
        response = self.request_manager.take_get_request(url, proxy_using=False)
        parseSourceCode = self.source_code_manager.parse_code(response['source_code'])
        last_page = self.find_last_page(parseSourceCode)
        while True:
            url = self.__createUrl(sourceUrl, page)
            print url
            try:
                response = self.request_manager.take_get_request(url, proxy_using=False)
            except Exception as exc:
                print exc
                break
            parseSourceCode = self.source_code_manager.parse_code(response['source_code'])

            links = self.findLinks(parseSourceCode)
            print len(links)
            if len(links) == 0:
                continue
            urls_data = self.generator.generate_urls_data(urls_list=links, proxy=response['proxy'])
            self.db.insert_urls(urls_list=urls_data,
                                session_id=self.session_id,
                                project_id=self.PROJECT_ID)
            if self.isLastPage(page, last_page):
                print "last"
                break
            page += 1

    def find_last_page(self, code):
        pagination = code.find('div', {'class':'paging '})
        pages = pagination.findAll('a')
        last_page = int(pages[-2].text)
        print  last_page
        return last_page

    def isLastPage(self, current_page, last_page):
        if current_page != last_page:
            return False
        else:
            return True


TEMPLATE_URL = "https://www.movoto.com/for-sale/" \
               "waller-county-tx/" \
               "price-500000-0/p-{}/" \
               "@29.9751629,-96.02557379999996"

if __name__ == '__main__':
    linksExtractor = LinksExtractor()
    linksExtractor.main(TEMPLATE_URL)
