
from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator

class LinksExtractor:

    DOMAIN = 'www.homes.com'
    PROJECT_ID = 3

    def __init__(self):
        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)


    def __createUrl(self, templateUrl, page):
        #url = templateUrl[:-1] + str(page)
        url = templateUrl.format(page)
        return url

    def findLinks(self, sourceCode):
        links = []
        listOfTags = sourceCode.findAll('div', {'class': 'agentArea parentHover'})
        for block in listOfTags:
            tag_a = block.find('a')
            href = tag_a['href']
            url = self.DOMAIN + href
            links.append(url)
        return links

    def main(self, sourceUrl):
        page = 1
        while True:
            url = self.__createUrl(sourceUrl, page)
            print url
            try:
                response = self.request_manager.take_get_request(url, proxy_using=False)
            except Exception as exc:
                print exc
                break
            parseSourceCode = self.source_code_manager.parse_code(response['source_code'])

            links = self.findLinks(parseSourceCode)
            print links
            urls_data = self.generator.generate_urls_data(urls_list=links, proxy=response['proxy'])
            # self.db.insert_urls(urls_list=urls_data,
            #                     session_id=self.session_id,
            #                     project_id=self.PROJECT_ID)
            if self.isLastPage(url, response['url']):
                print "last"
                break
            page += 1

    def isLastPage(self, url, response_url):
        if url == response_url:
            return False
        else:
            return True


TEMPLATE_URL = "https://www.homes.com/real-estate-agents/pacific-palisades-ca/p{}/"

if __name__ == '__main__':
    linksExtractor = LinksExtractor()
    linksExtractor.main(TEMPLATE_URL)
