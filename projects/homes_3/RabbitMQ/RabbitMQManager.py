import json
import multiprocessing
import threading
import rabbitpy

from RabbitMQConnector import RabbitMQConnector
from projects.homes_3.LinksExtractor import LinksExtractor as homesDataExtractor


class RabbitMQManager():
    EXCHANGE = 'input'
    QUEUE = 'realtors_cities'
    ROUTING_KEY = 'cities'

    def __init__(self, project_id):
        self.connector = RabbitMQConnector()
        self.connection = self.connector.connect()
        self.kwargs = {'connection': self.connection}
        self.publishChannel = self.connection.channel()

        self.project_id = project_id

    def startConsuming(self):
        self.choose_project()
        with self.connection.channel() as channel:
            print "Channel"
            channel.prefetch_count(1)
            for message in rabbitpy.Queue(channel, self.QUEUE):
                try:
                    bodyMessage = message.body
                    #bodyMessage = json.loads(bodyMessage)
                    print (" [x] %r received %r" % (threading.currentThread(), bodyMessage))
                    result = self.processMessage(bodyMessage)
                    if result is False:
                        self.load_urls([bodyMessage])
                    message.ack()
                except Exception as exc:
                    print exc

                    message.ack()

    def load_urls(self, urls):
        with self.connection.channel() as channel:
            for url in urls:
                message = rabbitpy.Message(channel=channel,
                                               body_value=url,
                                               properties={
                                                           "delivery_mode": 2},
                                               )
                message.publish(self.EXCHANGE, self.ROUTING_KEY)

    def choose_project(self):
        if self.project_id == 3:

            self.domainExtractor = homesDataExtractor()


    def processMessage(self, message):
        try:
            self.domainExtractor.main(message)
            return True
        except Exception as exc:
            print exc
            return False

def multiprocess(project_id):
    rabbit = RabbitMQManager(project_id)
    rabbit.startConsuming()




if __name__ == "__main__":
    multiprocess(project_id=3)