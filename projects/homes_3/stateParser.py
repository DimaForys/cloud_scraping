import threading

from LinksExtractor import LinksExtractor
from bs4 import BeautifulSoup
from RabbitMQ.RabbitMQManager import RabbitMQManager
import requests


def parse_states(state_list):
    rmq = RabbitMQManager(project_id=3)

    for state in state_list:
        cities_list = find_cities(state)
        rmq.load_urls(cities_list)
        #parse_cities(cities_list)

def parse_cities(cities_list):

    # pool = Pool(5)
    # pool.map(LinksExtractor().main, cities_list)
    for city in cities_list:
        LinksExtractor().main(city)


def find_cities(state_url):
    response = requests.get(state_url).text
    soup = BeautifulSoup(response, 'lxml')
    blocks_ = soup.findAll('ol')



    blocks = blocks_[1:]
    cities_list = []
    for block in blocks:
        cities = block.findAll('a')
        for city in cities:
            print city.text
            url = 'https://www.homes.com' + city['href'].strip() + 'p{}/'
            cities_list.append(url)
    return cities_list

def generate_cities_list(cities_list):
    for city in cities_list:
        print city



if __name__ == '__main__':
    parse_states(['https://www.homes.com/real-estate-agents/california/'])