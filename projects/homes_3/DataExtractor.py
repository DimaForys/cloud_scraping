import re

from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator




class DataExtractor:

    DOMAIN = 'www.homes.com'
    PROJECT_ID = 3

    def __init__(self):
        print self.DOMAIN
        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)

    def extract_data(self, url_data):
        url_id = url_data['id']
        url = url_data['url']
        url = 'https://'+url

        data = {}

        response = self.request_manager.take_get_request(url, proxy_using=False)
        source_code = response['source_code']
        parsed_code = self.source_code_manager.parse_code(source_code)
        proxy = response['proxy']

        agent_name = self.findAgentName(parsed_code)
        address = self.findAddress(parsed_code)
        email = self.findEmail(parsed_code)
        phone = self.findPhone(parsed_code)
        bread_crumbs = self.find_breacrums(parsed_code)


        data['email'] = email
        data['phone'] = phone
        data['address'] = address
        data['agent_name'] = agent_name
        data['broker'] = self.find_broker(parsed_code)
        data['state'] = bread_crumbs['state']
        data['city'] = bread_crumbs['city']

        data_for_db = self.generator.generate_data_to_database(data_from_page=data,
                                                               proxy=proxy,
                                                               url_id=url_id)
        print data_for_db
        self.db.insert_data(data_for_db, self.PROJECT_ID, self.session_name)
        self.db.set_url_processed(url_id)

    def find_breacrums(self, code):
        try:
            bread_crumbs = code.find('div', {'class': 'm breadcrumbs'})
            bread_crumbs = bread_crumbs.findAll('a')
            state = bread_crumbs[1].text
            city = bread_crumbs[2].text
        except:
            state = ''
            city = ''
        return {'state': state,
                'city': city}

    def find_broker(self, code):
        try:
            agentName = code.find('span', {'itemprop': 'branchOf'}).text
        except:
            agentName = ''
        return agentName.strip()

    def findAgentName(self, code):
        try:
            agentName = code.find('span', {'itemprop': 'name'}).text
        except:
            agentName = ''
        return agentName.strip()

    def findAddress(self, code):
        try:
            address = code.find('div', {'itemprop': 'address'}).text
        except AttributeError:
            address = ""
        return address.strip().replace('\n', '').replace('\t', ' ')

    def findEmail(self, code):
        try:
            match = re.search(r'[\w\.-]+@[\w\.-]+', str(code))
            email = match.group(0)

        except AttributeError:
            email = ""
        return email.strip()

    def findPhone(self, code):
        try:
            phone = code.find('li', {'itemprop': 'telephone'}).text
        except:
            phone = ''
        return phone.strip().replace('\n', '')



if __name__ == '__main__':
    url_data = {"url": "www.homes.com/real-estate-agents/veronica-aberham/id-23525028/", "id": 90721}
    DataExtractor().extract_data(url_data)