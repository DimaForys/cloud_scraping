import random
import time

from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator




class DataExtractor:

    DOMAIN = 'www.realtor.com'
    PROJECT_ID = 2

    def __init__(self):
        print self.DOMAIN
        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)
        # self.driver = self.request_manager.get_driver('chrome')
        # time.sleep(40)

    def __del__(self):
        print "Closing driver"
        #self.driver.quit()

    def extract_data(self, url_data):
        url_id = url_data['id']
        url = url_data['url']
        url = 'https://'+url
        time.sleep(random.uniform(0.8,2.5))
        data = {}
        #response = self.request_manager.take_js_request(url=url, driver=self.driver)
        response = self.request_manager.take_get_request(url=url, proxy_using=True)
        #print response
        source_code = response['source_code']
        parsed_code = self.source_code_manager.parse_code(source_code)
        proxy = response['proxy']

        agent_name = self.findAgentName(parsed_code)
        address = self.findAddress(parsed_code)
        price = self.findPrice(parsed_code)
        status = self.findStatus(parsed_code)
        days = self.findAmountOfDays(parsed_code)


        data['price'] = price
        data['status'] = status
        data['address'] = address
        data['agent_name'] = agent_name
        data['days'] = days

        data_for_db = self.generator.generate_data_to_database(data_from_page=data,
                                                               proxy=proxy,
                                                               url_id=url_id)
        print data_for_db
        self.db.insert_data(data_for_db, self.PROJECT_ID, self.session_name)
        self.db.set_url_processed(url_id)

    def findAmountOfDays(self, code):
        try:
            block = code.find(text='On realtor.com').parent.parent
            days = block.find('div', {'class': 'key-fact-data ellipsis'}).text.strip()
            days = int(days.split()[0])
            time = days
        except Exception as e:
            print e
            time = ''
        return time

    def findAgentName(self, code):
        try:
            agentName = code.find('span', {'data-label':'branding-agent-name'}).text
        except:
            agentName = ''
        return agentName.strip()

    def findAddress(self, code):
        try:
            address = code.find('div', {'class':'ldp-header-address'})['content']
        except AttributeError:
            address = ""
        return address.strip()

    def findPrice(self, code):
        try:
            price = code.find('span', {'itemprop':'price'}).text
        except AttributeError:
            price = ""
        return price.strip()

    def findStatus(self, code):
        try:
            status = code.find('div', {'class':'display-inline-block label label-gray-transparent color-base margin-right-sm'}).text
        except:
            status = ''
        return status.strip().replace('\n', '')

if __name__ == '__main__':
    import urlparse
    import httplib


    # Recursively follow redirects until there isn't a location header
    def resolve_http_redirect(url, depth=0):
        if depth > 10:
            raise Exception("Redirected " + str(depth) + " times, giving up.")
        o = urlparse.urlparse(url, allow_fragments=True)
        conn = httplib.HTTPConnection(o.netloc)
        path = o.path
        if o.query:
            path += '?' + o.query
        conn.request("HEAD", path)
        res = conn.getresponse()
        headers = dict(res.getheaders())
        if headers.has_key('location') and headers['location'] != url:
            return resolve_http_redirect(headers['location'], depth + 1)
        else:
            return url

    print resolve_http_redirect('https://www.realtor.com/realestateandhomes-detail/16001-Collins-Ave-Apt-3701_Sunny-Isles-Beach_FL_33160_M53765-22677?ex=FL635926598')
