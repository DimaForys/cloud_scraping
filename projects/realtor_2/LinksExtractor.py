import time

from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator

class LinksExtractor:

    DOMAIN = 'www.realtor.com'
    PROJECT_ID = 2

    def __init__(self):
        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)
        self.driver = self.request_manager.get_driver('chrome')


    def __createUrl(self, templateUrl, page):
        #url = templateUrl[:-1] + str(page)
        url = templateUrl.format(page)
        return url

    def getNumberOfLastPage(self, code):
        self.lastPage = code.findAll('span', {'class': 'page'})[-1]
        self.lastPage = self.source_code_manager.get_text(self.lastPage)
        self.lastPage = int(self.lastPage)

    def findLinks(self, sourceCode):
        links = []
        listOfTags = sourceCode.findAll('div', {'class': 'srp-item-address ellipsis'})
        for block in listOfTags:
            tag_a = block.find('a')
            href = tag_a['href']
            url = self.DOMAIN + href
            links.append(url)
        return links

    def main(self):
        sourceUrl = TEMPLATE_URL
        page = 167
        time.sleep(40)
        while True:
            url = self.__createUrl(sourceUrl, page)
            print url
            try:
                # response = self.request_manager.take_get_request(url, proxy_using=True)
                response = self.request_manager.take_js_request(url=url, driver=self.driver)

            except Exception as exc:
                print exc
                break
            parseSourceCode = self.source_code_manager.parse_code(response['source_code'])

            self.getNumberOfLastPage(parseSourceCode)

            links = self.findLinks(parseSourceCode)
            urls_data = self.generator.generate_urls_data(urls_list=links, proxy=response['proxy'])
            self.db.insert_urls(urls_list=urls_data,
                                session_id=self.session_id,
                                project_id=self.PROJECT_ID)
            if self.isLastPage(links, page):
                print "last"
                break
            page += 1

    def isLastPage(self, result, currentPage):
        if len(result) == 0:
            return True
        elif currentPage == self.lastPage:
            return True
        else:
            return False


TEMPLATE_URL = "https://www.realtor.com/realestateandhomes-search/Miami_FL/price-500000-na/radius-20/sby-6/pg-{}?pgsz=50"

if __name__ == '__main__':
    PRICE = 500000
    LOCATION = ''
    linksExtractor = LinksExtractor()
    linksExtractor.main()
