import json
import random
import re
import time

from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator




class DataExtractor:

    DOMAIN = 'clutch.com'
    PROJECT_ID = 7

    def __init__(self):
        print self.DOMAIN
        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)
        # self.driver = self.request_manager.get_driver('chrome')
        # time.sleep(40)

    def __del__(self):
        print "Closing driver"
        #self.driver.quit()

    def extract_data(self, url_data):
        url_id = url_data['id']
        url = url_data['url']
        url = 'https://' + url
        time.sleep(random.uniform(0.8,2.5))
        data = {}
        response = self.request_manager.take_get_request(url=url, proxy_using=False)
        source_code = response['source_code']
        parsed_code = self.source_code_manager.parse_code(source_code)
        proxy = response['proxy']

        data['website'] = self.find_website(parsed_code)
        data['email'] = self.find_email(parsed_code)
        data['name'] = self.find_name(parsed_code)
        data['phone'] = self.find_phone(parsed_code)
        #data['address'] = None


        data_for_db = self.generator.generate_data_to_database(data_from_page=data,
                                                               proxy=proxy,
                                                               url_id=url_id)
        print data
        self.db.insert_data(data_for_db, self.PROJECT_ID, self.session_name)
        self.db.set_url_processed(url_id)

    def find_website(self, source_code):
        website = source_code.find('li', {'class': 'quick-menu-element website-link-a'})
        website = website.find('a')['href']
        return website

    def find_email(self, source_code):
        email = source_code.find('div', {'class': 'field field-name-field-pp-email field-type-email field-label-hidden'})
        try:
            script = email.find('script')
        except AttributeError:
            return ''
        print script
        email = re.findall(re.compile('\'.+@.+\''), str(script))

        emails_parts = email[0].replace("'", '').split("#")
        formula = re.findall(re.compile('innerHTML.+;'), str(script))[0]
        list_of_numbers = re.findall(re.compile('\d'), str(formula))
        email = ''
        for number in list_of_numbers:
            email += emails_parts[int(number)]

        return email

    def find_name(self, source_code):
        name = source_code.find('h1', {'class': 'page-title'}).text
        return name.strip()

    def find_phone(self, source_code):
        try:
            phone = source_code.find('span', {'class': 'contact-dropdown-phone-ico'}).text
        except AttributeError:
            phone = ''
        return phone

    def find_address(self, source_code):
        pass





if __name__ == '__main__':
    DataExtractor().extract_data({u'url': u'clutch.co/profile/tpx-communications-formerly-dcsi', u'id': 1993485})