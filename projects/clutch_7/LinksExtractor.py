import time

from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator

class LinksExtractor:

    DOMAIN = 'clutch.co'
    PROJECT_ID = 7

    def __init__(self):
        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)


    def __createUrl(self, templateUrl, page):
        #url = templateUrl[:-1] + str(page)
        url = templateUrl.format(page)
        return url

    def findLinks(self, sourceCode):
        links = []
        listOfTags = sourceCode.findAll('h3', {'class': 'company-name'})
        for block in listOfTags:
            tag_a = block.find('a')
            href = tag_a['href']
            url = self.DOMAIN + href
            links.append(url)
        return links

    def main(self):
        sourceUrl = TEMPLATE_URL
        page = 0

        while True:
            url = self.__createUrl(sourceUrl, page)
            print url
            try:
                response = self.request_manager.take_get_request(url, proxy_using=False)
            except Exception as exc:
                print exc
                break
            parseSourceCode = self.source_code_manager.parse_code(response['source_code'])

            links = self.findLinks(parseSourceCode)
            urls_data = self.generator.generate_urls_data(urls_list=links, proxy=response['proxy'])
            self.db.insert_urls(urls_list=urls_data,
                                session_id=self.session_id,
                                project_id=self.PROJECT_ID)
            if self.isLastPage(links):
                print "last"
                break
            page += 1

    def isLastPage(self, result):
        if len(result) == 0:
            return True
        else:
            return False


TEMPLATE_URL = "https://clutch.co/it-services?field_pp_cs_small_biz_value=&field_pp_cs_midmarket_value=&field_pp_cs_enterprise_value=&field_pp_if_advertising_value=&field_pp_if_automotive_value=&field_pp_if_arts_value=&field_pp_if_bizservices_value=&field_pp_if_conproducts_value=&field_pp_if_education_value=&field_pp_if_natural_resources_value=&field_pp_if_finservices_value=&field_pp_if_gambling_value=&field_pp_if_gaming_value=&field_pp_if_government_value=&field_pp_if_healthcare_value=&field_pp_if_hospitality_value=&field_pp_if_it_value=&field_pp_if_legal_value=&field_pp_if_manufacturing_value=&field_pp_if_media_value=&field_pp_if_nonprofit_value=&field_pp_if_realestate_value=&field_pp_if_retail_value=&field_pp_if_telecom_value=&field_pp_if_transportation_value=&field_pp_if_utilities_value=&field_pp_if_other_value=&country=us&state=&distance%5Bpostal_code%5D=&distance%5Bcountry%5D=us&distance%5Bsearch_distance%5D=100&distance%5Bsearch_units%5D=mile&page={}"

if __name__ == '__main__':
    PRICE = 500000
    LOCATION = ''
    linksExtractor = LinksExtractor()
    linksExtractor.main()
