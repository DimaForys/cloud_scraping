import urllib2

from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator

class LinksExtractor:

    DOMAIN = 'www.brightmlshomes.com'
    PROJECT_ID = 1

    def __init__(self):
        self.middle = 4


        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)

    def _request(self, min_price, max_price):
        response = urllib2.urlopen(
            'http://www.brightmlshomes.com/Include/AJAX/MapSearch/GetListingPins.aspx?searchoverride=170849e8-b0f9-4ccc-a2f7-79dd00af7c37&ts=1510132395262&',
            data='Criteria%2FMinPrice=%24{}%2C000&Criteria%2FMaxPrice=%24{}%2C000&Criteria%2FFilterByAddress=1&Criteria%2FListingTypeID=1&Criteria%2FStatus=1%2C0&grp_rangetype=A&inrixDuration=15&inrixTod=8%3A00&Groups%2FGroup_OwnershipType=1&HardCodedCriterion=1899&Groups%2FGroup_AirCon=1&Groups%2FGroup_Pool=1&Groups%2FGroup_Style=1&Groups%2FGroup_Level=1&Groups%2FGroup_Room=1&Criteria%2FLocationJson=%5B%5D&Criteria%2FSearchMapNELat=41.01604090013302&Criteria%2FSearchMapNELong=-74.92950057983401&Criteria%2FSearchMapSWLat=37.591243145611884&Criteria%2FSearchMapSWLong=-78.22539901733401&Criteria%2FZoom=8&Criteria%2FSearchMapStyle=r&IgnoreMap=false&ListingSortID=29&view=map&first=0&Criteria%2FSearchType=map&SearchTab=mapsearch-criteria-basicsearch&CLSID=-1&ResultsPerPage=12'.format(
                str(min_price), str(max_price)))
        return response.read()

    def __create_filter_price(self):
        if self.min_price > 1000 and self.min_price < 4000:
            self.middle = 50
        elif self.min_price > 4000 and self.min_price < 6000:
            self.middle = 2000
        elif self.min_price > 6000 and self.min_price < 15000:
            self.min_price = 19000
        elif self.min_price > 25000:
            return False


    def __generate_URL(self, href):
        url = self.DOMAIN + href
        return url

    def __get_links(self, json_data):
        urls_list = []
        listings = json_data['lst']
        for listing in listings:
            href = listing['url']
            url = self.__generate_URL(href)
            urls_list.append(url)
        return urls_list

    def __process_filter(self):
        pass

    def main(self):
        min = 500
        max = 504
        middle = 3
        while True:
            if min > 1000 and min < 4000:
                middle = 30
            elif min > 4000 and min < 6000:
                middle = 1500
            elif min > 6000 and min < 15000:
                middle = 19000
            elif min > 25000:
                break
            print min, max

            response = self._request(min,max)
            json_response = self.source_code_manager.to_json(response)
            links = self.__get_links(json_response)
            print len(links)

            urls_data = self.generator.generate_urls_data(urls_list=links, proxy='localhost')
            self.db.insert_urls(urls_list=urls_data,
                                session_id=self.session_id,
                                project_id=self.PROJECT_ID)
            min = max
            max = max + middle



if __name__ == '__main__':
    print LinksExtractor().main()
