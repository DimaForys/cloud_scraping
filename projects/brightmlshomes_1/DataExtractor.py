

from database.DatabaseManager import DatabaseManager
from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator




class DataExtractor:

    DOMAIN = 'www.brightmlshomes.com'
    PROJECT_ID = 1

    def __init__(self):
        print self.DOMAIN
        self.middle = 4

        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)

    def extract_data(self, url_data):
        print url_data
        url_id = url_data['id']
        url = url_data['url']

        data = {}

        response = self.request_manager.take_get_request(url)
        source_code = response['source_code']
        parsed_code = self.source_code_manager.parse_code(source_code)
        proxy = response['proxy']

        status = parsed_code.find('span', attrs={'clas': 'ld-status'})  # "clas" this is not a mistake
        street = parsed_code.find('span', attrs={'class': 'full-address'})
        city = parsed_code.find('span', attrs={'class': 'city-state-zip'})
        price = self.find_price(parsed_code)
        agent_name = parsed_code.find('span', attrs={'class': 'listing-contact-name'})
        agent_company = parsed_code.find('div', attrs={'class': 'maininfo_officename'})
        agent_email = parsed_code.find('a', attrs={'class': 'listedby_email'})
        agent_phone = parsed_code.find('span', attrs={'class': 'listedby_phone'})
        number = parsed_code.find('div', attrs={'class': 'listing-number'})
        first_date = parsed_code.find('div', {'class': 'details-text-data pull-right'})


        status = self.source_code_manager.get_text(status)
        number = self.source_code_manager.get_text(number)
        street = self.source_code_manager.get_text(street)
        agent_name = self.source_code_manager.get_text(agent_name)
        city = self.source_code_manager.get_text(city)
        agent_company = self.source_code_manager.get_text(agent_company)
        agent_email = self.source_code_manager.get_text(agent_email)
        agent_phone = self.source_code_manager.get_text(agent_phone)
        first_date = self.source_code_manager.get_text(first_date)

        data['price'] = price
        data['status'] = status
        data['street'] = street
        data['city'] = city
        data['agent_name'] = agent_name
        data['agent_company'] = agent_company
        data['agent_email'] = agent_email
        data['agent_phone'] = agent_phone
        data['date'] = first_date
        data['listing_number'] = number
        print data

        data_for_db = self.generator.generate_data_to_database(data_from_page=data,
                                                               proxy=proxy,
                                                               url_id=url_id)
        self.db.insert_data(data_for_db, self.PROJECT_ID, self.session_name)
        self.db.set_url_processed(url_id)

    def find_price(self, code):
        prices = code.findAll('span', attrs={'class': 'price'})
        price_string = ''
        for price_ in prices:
            price = self.source_code_manager.get_text(price_)
            price_string += price
        return price_string



import multiprocessing

if __name__ == '__main__':
    extractor = DataExtractor()
    urls_list = DatabaseManager().get_urls(extractor.PROJECT_ID)
    print len(urls_list)
    the_queue = multiprocessing.Queue()

    def worker_main(queue):
        while True:
            item = queue.get(True)
            extractor.extract_data(item)

    the_pool = multiprocessing.Pool(10, worker_main, (the_queue,))
    for i in urls_list:
        the_queue.put(i)

