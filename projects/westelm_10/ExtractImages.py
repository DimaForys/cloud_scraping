import time

from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator

class ExtractImages:

    DOMAIN = 'westelm.com'
    PROJECT_ID = 10

    def __init__(self):
        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)


    def __createUrl(self, templateUrl, page):
        #url = templateUrl[:-1] + str(page)
        url = templateUrl.format(page)
        return url

    def findLinks(self, sourceCode):
        links = []
        div = sourceCode.find('div', {'id': 'content'})
        if div is None:
            return []

        ul = div.find('ul', {'class': 'shop-list product-list'})

        li = []
        li.append(ul.findAll('li', {'class': 'product-cell hoverSwappable'}))
        li.append(ul.findAll('li', {'class': 'product-cell '}))

        bad_img = 'https://www.westelm.com/weimgs/ab/images/i/201805/0029/images/common/blank.gif'
        for side in li:
            try:
                for item in side:

                    product_block = item.find('div', {'class': 'toolbox-for-product-cell'}).find('a')
                    img_list = product_block.findAll('img')

                    for block in img_list:
                        href = block['src']
                        if str(href) == bad_img:
                            href = block['data-async-src']
                        print href
                        links.append(href)
            except Exception as e:
                print '2 ', e
                break

        return links


    def main(self, url):

        page = 1
        running_url = self.__createUrl(url, page)
        response = self.request_manager.take_get_request(running_url, proxy_using=False)
        parseSourceCode = self.source_code_manager.parse_code(response['source_code'])
        last_page = self.find_last_page(parseSourceCode)

        while True:
            if page == 2:
                print '2'
            running_url = self.__createUrl(url, page)
            print running_url
            try:
                response = self.request_manager.take_get_request(running_url, proxy_using=False)
            except Exception as exc:
                print '3 ', exc
                break

            if response['source_code'] == '':
                break

            parseSourceCode = self.source_code_manager.parse_code(response['source_code'])

            try:
                links = self.findLinks(parseSourceCode)
            except Exception as e:
                print '1 ', e
                print 'CHTOTO NIE TAK'
                break

            print 'Total amount of urls', len(links)
            if len(links) == 0:
                break

            if self.isLastPage(page, last_page):
                print "last"
                break

            page +=1
            # urls_data = self.generator.generate_urls_data(urls_list=links, proxy=response['proxy'])
            # duplicates =  self.db.insert_urls(urls_list=urls_data,
            #                     session_id=self.session_id,
            #                     project_id=self.PROJECT_ID)
            # print 'Total duplicates', duplicates
            #
            # if len(links) == duplicates['amount_of_existing']:
            #     print "DUPLICATES"
            #     break

    def find_last_page(self, code):
        pagination = code.find('div', {'class':'c-category-pagination pagination-component'})
        if pagination is None:
            return 1
        pages = pagination.find('li', {'class': 'typeB'})
        last_page = int(pages.text.split(' of ')[-1])
        print  last_page
        return last_page

    def isLastPage(self, current_page, last_page):
        if current_page != last_page:
            return False
        else:
            return True


TEMPLATE_URL = "https://www.westelm.com"

if __name__ == '__main__':
   extractImages = ExtractImages()
   extractImages.main('https://www.westelm.com/shop/furniture/all-living-room/?page={}')

    # linksExtractor = LinksExtractor()
    # for i in range(50, 100):
    #     TEMPLATE_URL = "https://www.globewest.com.au/browse?cat={}&limit=60&p=".format(str(i))
    #     linksExtractor.main(TEMPLATE_URL)
