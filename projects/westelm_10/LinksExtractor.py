import re

from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator

class LinksExtractor:

    DOMAIN = 'westelm.com'
    PROJECT_ID = 10

    def __init__(self):
        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)


    def __createUrl(self, templateUrl, page):
        #url = templateUrl[:-1] + str(page)
        url = templateUrl+str(page)
        return url


    def findAllSubCategories(self, sourceCode):

        content_categ = sourceCode.find('div', {'id': 'content'})

        if content_categ == '':
            return False

        row_sub_categories = content_categ.findAll('ul', {'class': 'shop-list category-list'})

        sub_categories_links = []
        for row in row_sub_categories:
            tegs = row.findAll('a')

            for teg in tegs:
                sub_categories_links.append(teg['href'])

        return sub_categories_links


        # urls = nav_menu.findAll(text=re.compile('All.+'))
        # for url in urls:
        #     print url.parent.parent.find('a')['href']
        #
        # categories = nav_menu.findAll('li', {'class': 'dropDown hoverFade category'})

        # print categories



    def findLinks(self, sourceCode):
        links = []
        div = sourceCode.find('div', {'class': 'container product-list'})
        if div is None:
            return []

        listOfTags = div.findAll('img')
        for block in listOfTags:
            href = block['src']
            if href in links:
                continue
            links.append(href)
        return links

    def main(self):

        categories_list = [
            'https://www.westelm.com/shop/furniture/?cm_type=gnav',
            'https://www.westelm.com/shop/rugs-windows/?cm_type=gnav',
            'https://www.westelm.com/shop/bedding/?cm_type=gnav',
            'https://www.westelm.com/shop/lighting/?cm_type=gnav',
            'https://www.westelm.com/shop/accessories-pillows/?cm_type=gnav',
            'https://www.westelm.com/shop/wall-decor-mirrors/?cm_type=gnav',
            'https://www.westelm.com/shop/dining-kitchen/?cm_type=gnav',
            'https://www.westelm.com/shop/outdoor/?cm_type=gnav',
            'https://www.westelm.com/shop/gifts/?cm_type=gnav'
        ]


        for url in categories_list:
            response = self.request_manager.take_get_request(url, proxy_using=False)
            parseSourceCode = self.source_code_manager.parse_code(response['source_code'])

            sub_categories_urls = self.findAllSubCategories(parseSourceCode)

            print len(sub_categories_urls)
            for sub_url in sub_categories_urls:

                try:
                    response = self.request_manager.take_get_request(sub_url, proxy_using=False)
                    parseSourceCode = self.source_code_manager.parse_code(response['source_code'])

                    sub_sub_categories = self.findAllSubCategories(parseSourceCode)
                except Exception as e:
                    print e
                    continue

                if sub_sub_categories == False :
                    print 'End of sub_cetegory'
                    urls_data = self.generator.generate_urls_data(urls_list=sub_sub_categories, proxy=response['proxy'])
                    duplicates = self.db.insert_urls(urls_list=urls_data,
                                                     session_id=self.session_id,
                                                     project_id=self.PROJECT_ID)
                    print 'Total duplicates', duplicates
                    continue

                print len(sub_sub_categories)

                for link in sub_sub_categories:
                    if link not in sub_categories_urls:
                        print link
                        # sub_categories_.append(link)



        # page = 1
        # url = self.__createUrl(sourceUrl, page)
        # response = self.request_manager.take_get_request(url, proxy_using=False)
        # parseSourceCode = self.source_code_manager.parse_code(response['source_code'])
        # last_page = self.find_last_page(parseSourceCode)
        # while True:
        #     url = self.__createUrl(sourceUrl, page)
        #     print url
        #     try:
        #         response = self.request_manager.take_get_request(url, proxy_using=False)
        #     except Exception as exc:
        #         print exc
        #         break
        #     parseSourceCode = self.source_code_manager.parse_code(response['source_code'])
        #
        #     links = self.findLinks(parseSourceCode)
        #     print 'Total amount of urls', len(links)
        #     if len(links) == 0:
        #         break
        #
        #     urls_data = self.generator.generate_urls_data(urls_list=links, proxy=response['proxy'])
        #     duplicates =  self.db.insert_urls(urls_list=urls_data,
        #                         session_id=self.session_id,
        #                         project_id=self.PROJECT_ID)
        #
        #     print 'Total duplicates', duplicates
        #
        #     if self.isLastPage(page, last_page):
        #         print "last"
        #         break
        #     elif len(links) == duplicates['amount_of_existing']:
        #         print "DUPLICATES"
        #         break
        #
        #
        #     page += 1

    def find_last_page(self, code):
        pagination = code.find('div', {'class':'toolbar__pager'})
        if pagination is None:
            return 1
        pages = pagination.find('span')
        last_page = int(pages.text.split(' / ')[-1])
        print  last_page
        return last_page

    def isLastPage(self, current_page, last_page):
        if current_page != last_page:
            return False
        else:
            return True


TEMPLATE_URL = "https://www.westelm.com/"

if __name__ == '__main__':

    linksExtractor = LinksExtractor()
    linksExtractor.main()
    # for i in range(1, 100):
    #     TEMPLATE_URL = "https://www.globewest.com.au/browse?cat={}&limit=60&p=".format(str(i))
    #     linksExtractor.main(TEMPLATE_URL)
