
from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator

class LinksExtractor:

    DOMAIN = 'globewest.com.au'
    PROJECT_ID = 9

    def __init__(self):
        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)


    def __createUrl(self, templateUrl, page):
        #url = templateUrl[:-1] + str(page)
        url = templateUrl+str(page)
        return url

    def findLinks(self, sourceCode):
        links = []
        div = sourceCode.find('div', {'class': 'container product-list'})
        if div is None:
            return []

        listOfTags = div.findAll('img')
        for block in listOfTags:
            href = block['src']
            if href in links:
                continue
            links.append(href)
        return links

    def main(self, sourceUrl):
        page = 1
        url = self.__createUrl(sourceUrl, page)
        response = self.request_manager.take_get_request(url, proxy_using=False)
        parseSourceCode = self.source_code_manager.parse_code(response['source_code'])
        last_page = self.find_last_page(parseSourceCode)
        while True:
            url = self.__createUrl(sourceUrl, page)
            print url
            try:
                response = self.request_manager.take_get_request(url, proxy_using=False)
            except Exception as exc:
                print exc
                break
            parseSourceCode = self.source_code_manager.parse_code(response['source_code'])

            links = self.findLinks(parseSourceCode)
            print 'Total amount of urls', len(links)
            if len(links) == 0:
                break

            urls_data = self.generator.generate_urls_data(urls_list=links, proxy=response['proxy'])
            duplicates =  self.db.insert_urls(urls_list=urls_data,
                                session_id=self.session_id,
                                project_id=self.PROJECT_ID)
            print 'Total duplicates', duplicates
            if self.isLastPage(page, last_page):
                print "last"
                break
            elif len(links) == duplicates['amount_of_existing']:
                print "DUPLICATES"
                break


            page += 1

    def find_last_page(self, code):
        pagination = code.find('div', {'class':'toolbar__pager'})
        if pagination is None:
            return 1
        pages = pagination.find('span')
        last_page = int(pages.text.split(' / ')[-1])
        print  last_page
        return last_page

    def isLastPage(self, current_page, last_page):
        if current_page != last_page:
            return False
        else:
            return True


TEMPLATE_URL = "https://www.globewest.com.au/browse?cat=14&limit=60&p={}"

if __name__ == '__main__':
   pass

    # linksExtractor = LinksExtractor()
    # for i in range(50, 100):
    #     TEMPLATE_URL = "https://www.globewest.com.au/browse?cat={}&limit=60&p=".format(str(i))
    #     linksExtractor.main(TEMPLATE_URL)
