import random
import time

from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator

import urllib
import os

class DataExtractor:

    DOMAIN = 'movoto.com'
    PROJECT_ID = 9

    def __init__(self):
        print self.DOMAIN
        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)



    def extract_data(self, url_data, debug=False):
        url_id = url_data['id']
        url = url_data['url']
        print url

        path = os.path.dirname(os.path.abspath(__file__))
        urllib.urlretrieve(url, path+'/photos/'+str(url_id)+'.jpg')

        if debug:
            return
        self.db.set_url_processed(url_id)



if __name__ == '__main__':
    DataExtractor().extract_data({'id':1,
                                  'url':'https://www.movoto.com/home/950-n-michigan-ave-apt-4205-chicago-il-60611-461_09349422'},
                                 debug=True)