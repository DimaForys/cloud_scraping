
from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator

class LinksExtractor:

    DOMAIN = 'realestate.co.nz'
    PROJECT_ID = 4

    def __init__(self):
        self.request_manager = RequestManager()
        self.driver = self.request_manager.get_driver('chrome')
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)


    def __createUrl(self, templateUrl, page):
        #url = templateUrl[:-1] + str(page)
        url = templateUrl.format(page * 80)
        return url

    def getNumberOfLastPage(self, code):
        self.lastPage = code.findAll('span', {'class': 'page'})[-1]
        self.lastPage = self.source_code_manager.get_text(self.lastPage)
        self.lastPage = int(self.lastPage)

    def findLinks(self, sourceCode):
        links = []
        listOfTags = sourceCode.findAll('h2', {'class': 'listing-tile-title'})
        for block in listOfTags:
            tag_a = block.find('a')
            href = tag_a['href']
            url = self.DOMAIN + href
            links.append(url)
        return links

    def main(self):
        sourceUrl = TEMPLATE_URL
        page = 34
        while True:
            url = self.__createUrl(sourceUrl, page)
            print url, page
            try:
                response = self.request_manager.take_js_request(driver=self.driver, url=url,
                                                                control_class='listing-tile-title',
                                                                control_amount=80)
            except Exception as exc:
                print exc
                break
            parseSourceCode = self.source_code_manager.parse_code(response)

            links = self.findLinks(parseSourceCode)
            print len(links), links
            urls_data = self.generator.generate_urls_data(urls_list=links, proxy=None)
            response_from_db = self.db.insert_urls(urls_list=urls_data,
                                session_id=self.session_id,
                                project_id=self.PROJECT_ID)
            if self.isLastPage(response_from_db):
                print "last"
                break
            elif page > 70:
                break
            page += 1
        self.driver.quit()


    def isLastPage(self, response):
        if response['amount_of_existing'] > 40:
            return True
        else: return False

TEMPLATE_URL = "https://www.realestate.co.nz/residential/sale?q=&qai=&by=date_desc&cat=1&qo={}&ql=80"

if __name__ == '__main__':
    PRICE = 500000
    LOCATION = ''
    linksExtractor = LinksExtractor()
    linksExtractor.main()
