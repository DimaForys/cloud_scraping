import json
import random
import re
import time

from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator




class DataExtractor:

    DOMAIN = 'miamirealtor.com'
    PROJECT_ID = 5

    def __init__(self):
        print self.DOMAIN
        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)
        # self.driver = self.request_manager.get_driver('chrome')
        # time.sleep(40)

    def __del__(self):
        print "Closing driver"
        #self.driver.quit()

    def extract_data(self, url_data):
        url_id = url_data['id']
        url = url_data['url']
        url = 'https://www.' + url
        url = url.replace('miamirealtor', 'miamirealestate')
        time.sleep(random.uniform(0.8,2.5))
        data = {}
        #response = self.request_manager.take_js_request(url=url, driver=self.driver)
        response = self.request_manager.take_get_request(url=url, proxy_using=False)
        #print response
        source_code = response['source_code']
        parsed_code = self.source_code_manager.parse_code(source_code)
        proxy = response['proxy']

        script_code = parsed_code.findAll('script')
        print response

        json_data =  self.get_json(str(script_code[-1]))


        agent_name = json_data['agent_name']

        agent_phone = json_data['agent_phone']
        agent_email = json_data['agent_email']
        city = json_data['city']
        board_name = json_data['board_name']
        address = json_data['address']
        price = json_data['list_price']
        days = json_data['days_on_market']
        complex = json_data['complex']


        data['price'] = price
        data['address'] = address
        data['agent_name'] = agent_name
        data['days'] = days
        data['agent_phone'] = agent_phone
        data['agent_email'] = agent_email
        data['city'] = city
        data['board_name'] = board_name
        data['complex'] = complex
        print data

        data_for_db = self.generator.generate_data_to_database(data_from_page=data,
                                                               proxy=proxy,
                                                               url_id=url_id)
        self.db.insert_data(data_for_db, self.PROJECT_ID, self.session_name)
        self.db.set_url_processed(url_id)

    def get_json(self, code):
        return json.loads(re.findall('{.+}', code)[0])


if __name__ == '__main__':
    DataExtractor().extract_data({u'url': u'miamirealtor.com/listings/264157331/Coral-Gables/135-Leucadendra-Dr', u'id': 1993485})