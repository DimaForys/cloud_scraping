import json
import random
import re
import time

from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator




class DataExtractor:

    DOMAIN = 'katielarsonrealtor.com'
    PROJECT_ID = 6

    def __init__(self):
        print self.DOMAIN
        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)
        # self.driver = self.request_manager.get_driver('chrome')
        # time.sleep(40)

    def __del__(self):
        print "Closing driver"
        #self.driver.quit()

    def extract_data(self, url_data):
        # url_id = url_data['id']
        # url = url_data['url']
        # time.sleep(random.uniform(0.8,2.5))
        # data = {}
        # response = self.request_manager.take_get_request(url=url, proxy_using=False)
        # #print response
        # source_code = response['source_code']
        # parsed_code = self.source_code_manager.parse_code(source_code)
        # proxy = response['proxy']
        # source_code = parsed_code
        #
        #
        # agent_name = self.get_agent_name(source_code)
        #
        # city = self.get_city(source_code)
        # address = self.get_address(source_code,city)
        # price = self.get_price(source_code)
        # days = self.get_days(source_code)
        # status = self.get_status(source_code)
        # value = self.get_value(source_code)
        # property_type = self.get_property_type(source_code)
        # property_subtype = self.get_property_subtype(source_code)
        # sqft = self.get_sqft(source_code)
        # agency = self.get_agency(source_code)
        # view = self.get_view(source_code)
        #
        #
        # data['price'] = price
        # data['address'] = address['address']
        # data['zip'] = address['zip']
        # data['agent_name'] = agent_name
        # data['days'] = days
        # data['city'] = city
        # data['status'] = status
        # data['value'] = value
        # data['property_type'] = property_type
        # data['property_subtype'] = property_subtype
        # data['sqft'] = sqft
        # data['agency'] = agency
        # data['view'] = view
        #
        # print data
        # data_for_db = self.generator.generate_data_to_database(data_from_page=data,
        #                                                        proxy=proxy,
        #                                                        url_id=url_id)
        self.db.insert_data(url_data, self.PROJECT_ID, self.session_name)
        print url_data
        # self.db.set_url_processed(url_id)


    def get_zip(self, address):
        zip = address.split(' ')[0]
        return zip

    def find_by_text(self, key_word, source_code):
        agent_name_block = source_code.find(text=key_word).parent.parent
        agent_name = agent_name_block.text
        return agent_name.replace(key_word, '').strip()

    def get_view(self, code):
        view = self.find_by_text(key_word='View:', source_code=code)
        return view


    def get_agent_name(self, code):
        agent_name = code.find(text=re.compile('Listing Agent:.+'))
        return agent_name.replace('Listing Agent:', '').strip()

    def get_city(self, code):
        key_word = 'County:'
        agent_name = self.find_by_text(key_word=key_word, source_code=code)
        return agent_name

    def get_address(self, code, city):
        address = code.find('h4', {'class':'white-text thin listing-address-header'}).text.strip()
        print address
        zip = address.split(' ')[-1]
        return {'address':address.split(city)[0],
                'zip':zip}

    def get_price(self, code):
        price = code.find('div', {'class':'listing-price'}).text.strip()
        return price

    def get_days(self, code):
        days = self.find_by_text(key_word='Days on market', source_code=code)
        return days

    def get_status(self, code):
        key_word = 'Status:'
        status = self.find_by_text(key_word=key_word, source_code=code)
        return status

    def get_value(self, code):
        value = self.find_by_text(key_word='MLS #:', source_code=code)
        return value

    def get_property_type(self, code):
        p_t = self.find_by_text(key_word='Property Type:', source_code=code)
        return p_t

    def get_property_subtype(self, code):
        p_st = self.find_by_text(key_word='Property Subtype:', source_code=code)
        return p_st

    def get_sqft(self, code):
        agent_name = code.find(text=re.compile('\d+ Sqft'))
        return agent_name.replace('Sqft', '').strip()

    def get_agency(self, code):
        agent_name = code.find(text=re.compile('Courtesy of:.+'))
        return agent_name.replace('Courtesy of:', '').strip()


if __name__ == '__main__':
    DataExtractor().extract_data({u'url': u'https://www.katielarsonrealtor.com/property/ca/90024/los-angeles/westwood--c04---bel-air---holmby-hills--holmby-hills--west-los-angeles/594-s-mapleton-drive/582d168ebb97521761001cc7/', u'id': 1993485})