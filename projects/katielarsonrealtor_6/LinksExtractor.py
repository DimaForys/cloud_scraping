import json
import random
import re
import time

from database.DatabaseManager import DatabaseManager

from managers.RequestManager import RequestManager
from managers.SourceCodeManager import SourceCodeManager
from managers.Generator import Generator

from RabbitMQ.RabbitMQManager import RabbitMQManager




class DataExtractor:

    DOMAIN = 'katielarsonrealtor.com'
    PROJECT_ID = 6

    def __init__(self):
        print self.DOMAIN
        self.request_manager = RequestManager()
        self.source_code_manager = SourceCodeManager()
        self.generator = Generator()
        self.db = DatabaseManager()
        self.session_name = self.generator.generate_session_name(project_id=self.PROJECT_ID)
        self.db.add_session(self.session_name, self.PROJECT_ID)
        self.session_id = self.db.get_session_id(session_name=self.session_name)

        self.rmq = RabbitMQManager(self.PROJECT_ID)
        # self.driver = self.request_manager.get_driver('chrome')
        # time.sleep(40)

    def __del__(self):
        print "Closing driver"
        #self.driver.quit()

    def extract_data(self, url_data):
        url_id = url_data['id']
        time.sleep(random.uniform(0.8,2.5))
        data = {}
        start = 2200
        break_ = False
        ids = []
        while True:
            print start

            url = url_data['url'].format(str(STEP), str(start))
            print url

            response = self.request_manager.take_get_request(url=url, proxy_using=False)
            #print response
            source_code = response['source_code']
            try:
                json_data = json.loads(source_code)
            except:
                print "ERROR"
                start += STEP
                continue
            proxy = response['proxy']
            organic_results = json_data['organic_results']
            search_results = organic_results['search_results']
            i = 1
            list = []

            for result in search_results:
                data = {}
                location = result['location']
                cur_data = result['cur_data']
                log_loc = result['log_localities']
                agency_data = result['agency_data']
                mongo_id = result['mongo_id']
                if mongo_id in ids:
                    print "EXIST", mongo_id
                    continue
                else:
                    ids.append(mongo_id)

                rets = result['rets']

                data['price'] = cur_data['price']
                if int(cur_data['price']) < 500000:
                    print "PRICE IS LOW"
                    break_ = True
                    break
                data['address'] = location['address']
                data['zip'] = location['postal']
                data['agent_name'] = rets['aname']
                data['days'] = cur_data['dom']
                data['city'] = location['locality']
                data['status'] = cur_data['status']
                data['value'] = rets['mls_id']
                data['property_type'] = cur_data['prop_type']
                #data['property_subtype'] = property_subtype
                data['sqft'] = cur_data['sqft']
                data['agency'] = rets['oname']
                data['agent_phone'] = rets['aphone']
                data['url'] = 'https://www.katielarsonrealtor.com/property/' + mongo_id

                data_for_db = self.generator.generate_data_to_database(data_from_page=data,
                                                                       proxy=proxy,
                                                                       url_id=1)
                #self.db.insert_data(data_for_db, self.PROJECT_ID, self.session_name)
                list.append(data_for_db)
                i += 1
            start += STEP
            self.rmq.load_urls(list)
            if break_:
                break


STEP = 100

if __name__ == '__main__':

    DataExtractor().extract_data({u'url': u'https://queryserviceb.placester.net/search?sort_field=price&sort_direction=desc&search_num_results={}&search_start_offset={}&q=%2Flistings-search%2F&purchase_types=buy&region_id=ca&origin_ids=53ea4a4c1cdabc4a5f000001', u'id': 1993485})