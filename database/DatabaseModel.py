from sqlalchemy import Column, ForeignKey, Integer, String, Text, DateTime, BOOLEAN, JSON, TIMESTAMP, FLOAT
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine

from config import database_uri

Base = declarative_base()

class Website(Base):
    __tablename__ = 'website'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    domain = Column(String(100))

class Url(Base):
    __tablename__ = 'url'
    id = Column(Integer, primary_key=True)
    url = Column(String(400))
    project_id = Column(Integer, ForeignKey('project.id'))
    processed = Column(BOOLEAN, default=False)
    hash = Column(String(100), unique=True)
    session_id = Column(Integer)
    proxie = Column(String(50))
    timestamp = Column(TIMESTAMP)

class Data(Base):
    __tablename__ = 'data'
    id = Column(Integer, primary_key=True)
    data_from_page = Column(JSON)
    project_id = Column(Integer, ForeignKey('project.id'))
    url_id = Column(Integer, ForeignKey('url.id'))
    proxie = Column(String(50))
    timestamp = Column(TIMESTAMP)
    session = Column(String(20))

class Proxie(Base):
    __tablename__ = 'proxie'
    id = Column(Integer, primary_key=True)
    protocol = Column(String(6))
    host = Column(String(50), unique=True)
    port = Column(String(10))
    dead = Column(BOOLEAN, default=False)
    black_list = Column(String(300))
    timestamp = Column(TIMESTAMP)

class Client(Base):
    __tablename__ = 'client'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    url = Column(String(100))

class Project(Base):
    __tablename__ = 'project'
    id = Column(Integer, primary_key=True)
    title = Column(String(50))
    url = Column(String(100))
    client_id = Column(Integer, ForeignKey('client.id'))
    website_id = Column(Integer, ForeignKey('website.id'))

class Session(Base):
    __tablename__ = 'session'
    id = Column(Integer, primary_key=True)
    name = Column(String(20), unique=True)
    project_id = Column(Integer, ForeignKey('project.id'))



def createTables():
    engine = create_engine(database_uri)
    Base.metadata.bind = engine
    Base.metadata.create_all(engine)

if __name__ == '__main__':
    createTables()



