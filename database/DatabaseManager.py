from contextlib import contextmanager
from sqlalchemy import create_engine
from sqlalchemy import update
from sqlalchemy.orm import sessionmaker

from database.DatabaseModel import Url
from database.DatabaseModel import Website
from database.DatabaseModel import Data
from database.DatabaseModel import Proxie
from database.DatabaseModel import Session
from database.DatabaseModel import Project

from helpers.timestamp_generator import generate_timestamp

from config import database_uri


class DatabaseManager():
    def __init__(self):
        self.dbPoolConnections = self.openConnection()

    def openConnection(self):
        myPool = create_engine(database_uri)
        return myPool

    def __del__(self):
        print "Disconnected"
        self.dbPoolConnections.dispose()

    @contextmanager
    def open_session(self):
        self.con = self.dbPoolConnections.connect()
        DBSession = sessionmaker(bind=self.con)
        session = DBSession()
        try:
            yield session
        finally:
            session.close()
            self.con.close()

    def insert_urls(self, urls_list, session_id, project_id):
        """:param urls_list - {'url': '', 'hash': '', 'proxie': ''}"""
        amount_of_existing = 0
        with self.open_session() as session:
            for url in urls_list:
                url_ = url['url']
                hash = url['hash']
                proxie = url['proxy']
                timestamp = generate_timestamp()
                new_url = Url(url=url_,
                              project_id=project_id,
                              processed=False,
                              hash=hash,
                              session_id=session_id,
                              proxie=proxie,
                              timestamp=timestamp)
                try:
                    session.add(new_url)
                    session.flush()
                    session.commit()
                except Exception as exc:
                    amount_of_existing += 1
                    session.rollback()
        return {'amount_of_existing': amount_of_existing}

    def insert_data(self, data, project_id, session_name):
        with self.open_session() as session:
            data_from_page = data['data']
            url_id = data['url_id']
            proxie = data['proxy']
            timestamp = generate_timestamp()
            new_row = Data(data_from_page=data_from_page,
                               project_id=project_id,
                               url_id=url_id,
                               proxie=proxie,
                               timestamp=timestamp,
                           session=session_name)
            session.add(new_row)
            session.commit()

    def get_urls(self, project_id):
        urls_list = []
        with self.open_session() as session:
            urls = session.query(Url).filter(Url.processed == False,
                                             Url.project_id == project_id
                                             ).all()
            for url in urls:
                urls_list.append({'url': url.url,
                                  'id': url.id})
            return urls_list

    def set_url_processed(self, url_id):
        with self.open_session() as session:
            smtp = update(Url).where(Url.id == url_id). \
                values(processed=True)
            session.execute(smtp)
            session.commit()

    def get_website_domain(self, website_id):
        with self.open_session() as session:
            domain_object = session.query(Website).filter(Website.id == website_id).first()
            domain = domain_object.domain
            return domain

    def insert_proxies(self, proxies_list):
        with self.open_session() as session:
            for proxie in proxies_list:
                host = proxie['host']
                protocol = proxie['protocol']
                port = proxie['port']
                timestamp = generate_timestamp()
                new_row = Proxie(protocol=protocol,
                                 host=host,
                                 port=port,
                                 timestamp=timestamp)
                session.add(new_row)
                session.commit()

    def get_proxies(self):
        proxies_list = []
        with self.open_session() as session:
            proxies = session.query(Proxie).filter(Proxie.dead == False).all()
            for proxie in proxies:
                proxies_list.append({'id': proxie.id,
                                     'port': proxie.port,
                                     'protocol': proxie.protocol,
                                     'host': proxie.host})
            return proxies_list

    def set_proxie_dead(self, proxie_id):
        with self.open_session() as session:
            smtp = update(Proxie).where(Proxie.id == proxie_id). \
                values(dead=True)
            session.execute(smtp)
            session.commit()

    def add_proxie_in_black_list(self, proxie_id, project_id):
        with self.open_session() as session:
            proxie = session.query(Proxie).filter(Proxie.id == proxie_id).first()
            black_list = proxie.black_list
            if black_list is None:
                black_list = ''
            smtp = update(Proxie).where(Proxie.id == proxie_id). \
                values(black_list + project_id + ',')
            session.execute(smtp)
            session.commit()

    def get_session_name(self, session_id):
        with self.open_session() as session:
            session_ = session.query(Session).filter(Session.id == session_id).first()
            session_ = session_.name
            return session_

    def get_session_id(self, session_name):
        with self.open_session() as session:
            session_ = session.query(Session).filter(Session.name == session_name).first()
            return session_.id

    def add_session(self, session_name, project_id):
        with self.open_session() as session:
            try:
                new_session = Session(name=session_name,
                                      project_id=project_id,
                                      )
                session.add(new_session)
                session.commit()
            except Exception as exc:
                session.rollback()
                pass

    def get_project_name(self, project_id):
        with self.open_session() as session:
            project_name = session.query(Project.title).filter(Project.id == project_id).first()
            return project_name[0]

    def get_data(self, project_id, session_name):
        with self.open_session() as session:

            data_list = []
            object_list = session.query(Data, Url).join(Url).filter(Data.project_id == project_id,
                                                                    Data.session == session_name).all()
            for object in object_list:
                item_data = {}
                data = object[0]
                url = object[1]
                item_data['data'] = data.data_from_page
                item_data['timestamp'] = data.timestamp
                item_data['url'] = url.url
                data_list.append(item_data)
        return data_list


if __name__ == '__main__':
    print DatabaseManager().get_data(1)